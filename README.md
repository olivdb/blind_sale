# WIDG Blind Token Sale

Smart Contracts allowing the blinded sale of 1 million ERC-20 tokens. Buyers submit secret bids during the Bidding Period and subsequently reveal them during the Reveal Period. At the end of the Reveal Period, the admin distributes the tokens using the maximum possible sale price that allows all 1 million tokens to be sold.

## Structure

Two contracts are used. WIDGToken.sol implements the ERC-20-compliant token management logic. WIDGTokenSale.sol handles the blinded sale of the tokens.

## Blinded Sale Contract

### Phases of the Sale

The Sale can be divided in multiple phases:

_1. Token Provisioning Phase_

After the sale contract is created, the admin who created the contract should manually provision the sale contract with the tokens to be issued during the sale. There is no time limit for this phase. This phase ends when the admin calls `startAuction()`. The total number of tokens issued in the sale is the total number of tokens owned by the sale contract at the time when `startAuction()` is called.

_2. Bidding Phase_

The Bidding Phase starts immediately after the admin calls `startAuction()`. When calling `startAuction()`, the admin specifies the duration of the Bidding Phase through the function parameter `biddingTime`. During the Bidding Phase, anyone can submit a blinded bid to purchase a certain quantity of tokens. A blinded bid comprises some ether deposit along with the hash of the concatenation of the maximum price to be paid for a token, the desired amount of ether to spend, and a secret key.

_3. Reveal Phase_

The Reveal Phase starts immediately after the end of the Bidding Phase. When calling `startAuction()`, the admin specifies the duration of the Reveal Phase through the function parameter `revealTime`. During the Reveal Phase, bidders send their bids (defined as the maximum price to be paid for a token, the ether commitment, and a secret key) in clear and the contract checks whether the hashes of these bids match the hashes recorded during the Bidding Phase. If the hashes match and the bid's ether commitment is greater than zero and the ether deposit is greater than the ether commitment, the bid is considered valid and is stored for further processing after the end of the Reveal Phase. If the hashes match but the bid's ether commitment is zero or the ether deposit is lower than the ether commitment, the bid is considered invalid (or "faked") and is immediately refunded to the bidder.

Bids that are revealed after the end of the Reveal Phase are immediately refunded to the bidder, regardless of their validity.

_4. Token Distribution and Refund Phase_

After the Reveal Phase has ended, the admin calls `distributeTokens()` to perform the distribution of tokens. A sale price is calculated as the price that allows the largest possible amount of ether commitment to be fulfilled for the sale of all 1 million tokens. The tokens are then distributed to successful bidders and their ether commitments are withdrawn to the admin account. Unsuccessful bidders are now allowed to withdraw their deposited ether by calling `withdrawRefund()`.

### Blinding the bids

During the Bidding Phase, bidders send only hashes of their bids (a bid comprises the maximum price to be paid for a token, the ether commitment, and a secret key), along with some ether deposit. Requiring bidders to send some ether deposit with their blinded bids makes it so that the blinded bids are _binding_. Unfortunately, because the ether deposits are publically visible on the blockchain, if we required the ether deposits to exactly match the ether commitments, the sale would no longer be blinded. To circumvent this problem, we allow deposits to be lower, equal, or greater than the blinded ether commitments. During the Reveal Phase, if a deposit is revealed to be lower than the ether commitment of the bid, the bid is considered "fake" and its deposit is immediately refunded to the bidder. If a deposit is revealed to be greater than the ether commitment of the bid, the bid is considered valid but the excess ether is immediately refunded to the bidder. As a result, one gains no valuable information by inspecting the ether deposits since a deposit of any size can be faked by associating it with an ether commitment greater or lower than the amount of ether deposited.

### Revealing the bids

A bidder can reveal all their bids at once by passing an array of prices, ether commitments and secrets to `revealBids()`. While this is convenient for the bidder who needs to only perform one network transaction, it was observed during testing that bidders trying to reveal large arrays of bids (~40 bids or more) may hit the _block gas limit_. For this reason, we added the option to specify a `startingIndex` when calling `revealBids()`, which allows bidders having placed many bids to only reveal a subset of their bids in one transaction.


### Calculating the Sale Price

The sale price is calculated as follows:

1. Build the list of valid bids. In the code, this list is stored in the state variable `RevealedBid[] public revealedBids` and is constructed during the Reveal Period with the bids `b` that were successfully revealed, have an ether commitment `b.etherSpent` greater than zero and an ether deposit `b.deposit` greater or equal to the ether commitment `b.etherSpent`.
2. Order all `N` valid bids by descending prices.
3. Step through these ordered bids to find the first bid for which the price is lower than the price `P` obtainted by dividing
the cumulated ether commitment of the previous `M` bids by the total number of tokens to sell. This calculated price `P` is the sale price.

The first `M` bids in the ordered list can be considered successful and will be fulfilled at the sale price. The last `N-M` bids of the ordered list can be considered unsuccessful and will be refunded their ether commitments.

### Ordering the revealed bids

As indicated in the previous section, calculating the sale price requires sorting the array of valid bids by descending prices. Several approaches have been considered with regard to sorting the revealed bids:

_1. Off-chain Sorting_

Since sorting on the EVM is a potentially expensive operation, we first considered whether the sorting of the revealed bids could be done off-chain. We noted that the problem of verifying that an input array is the sorted version of another array presents a similar time and space complexity to the problem of sorting the array to begin with. On-chain verification of the correctness of the off-chain sorting is therefore not advisable. Hence, sorting the revealed bids off-chain only makes sense if it is acceptable for the contract to receive the sale price from the admin instead of calculating it itself by sorting the revealed bids on-chain. This implies that bidders would need to trust the admin to calculate the sale price correctly. The contract would still guarantee that no bid gets fulfilled at a price greater than its maximum price and that all the allocated tokens get sold. It seems reasonable for the bidders to trust the admin to select the highest possible sale price given these guarantees since this would maximize the amount of ether collected by the admin.

_2. Storing the revealed bids in a Binary Search Tree_

Instead of requiring the admin to pay a gas cost that grows as `O(N*log(N))` to sort the `N` revealed bids in `distributeTokens()`, we could share this cost between the `N` bids and have each bidder pay a gas cost growing as `O(log(N))` whenever they reveal a bid during the Reveal Phase so that the admin only need to pay a gas cost that grows as `O(N)`. This could be achieved by using a Binary Search Tree to store the revealed bids instead of an array (see this [Red-Black Tree](https://github.com/AtlantPlatform/rbt-solidity) implementation for instance). One benefit of this approach is that we reduce the risk of the admin hitting a _block gas limit_ in `distributeTokens()`. But this approach presents the inconvenience of significantly increasing the complexity of the implementation by using a non-trivial data structure. It also increases transaction fees for all bidders, who need to write to `O(log(N))` `storage` locations for each bid, which is probably not desirable considering that `storage` is the most expensive type of memory space in the EVM.

_3. Sorting the revealed bids_

In this approach, the admin sorts the revealed bids on-chain in `distributeTokens()`. Using the Quicksort sorting algorithm, the ordering can be performed using an average time complexity of `O(Nlog(N))` (`O(N^2)` in the worst-case) and additional space complexity (stack size) of `O(log(N))`. Heapsort is also an interesting candidate sorting algorithm, giving a worst-case time complexity of `O(Nlog(N))` and constant additional space complexity. Both these algoritms were implemented for our tests (the code for Heapsort is found in `WIDGTokenSaleMock.sol`). In our tests, Quicksort was found to use significantly less gas than Heapsort (up to 50% less for large bid arrays). 

We also compared doing the sorting in `storage` versus copying the array of revealed bids to `memory` and sort it in there. Writing to `storage` is generally more expensive than writing to `memory` and we found in our tests that running Quicksort in `memory` indeed reduces the gas cost by an order of magnitude compared to running it in `storage`.

One can compare the gas used by in-memory Quicksort, in-memory Heapsort and in-storage Quicksort by setting `compareSortingFunctions` to `true` in `test/WIDGTokenSale.js`.

_Design choice_

We decided to use Quicksort in-memory to sort the revealed bids in `distributeTokens()` given that this approach requires less gas from bidders while still limiting the amount of gas spent by the admin account to a reasonable level.

One limitation of our approach is that sorting the array of revealed bids may hit the _block gas limit_ for very large number of bids. For production code, we would therefore recommend to adopt an approach where the sorting of the array of revealed bids can be broken down in multiple transactions.

### Withdrawing refunds

We use the [Withdrawal Pattern](http://solidity.readthedocs.io/en/v0.4.24/common-patterns.html#withdrawal-from-contracts) to implement the withdrawal of unsuccessful bids after the end of the token distribution. This pattern makes sure that a malicious contract cannot prevent the token distribution function from executing properly.

## Tests

To run the tests, just run the following command:

    $ truffle test 

The tests use the mock contract in `contracts/mocks/WIDGTokenSaleMock.sol` that inherit from `WIDGTokenSale.sol` but adds the following functionalities:

1. The ability to travel through time by directly modifying the variables `biddingEnd` and `revealEnd`. This was found to be a simpler and more flexible solution than using the testRPC command `evm_increaseTime`.
2. The implementation of two additional sorting algorithms (in-memory Heapsort and in-storage Quicksort), to compare their gas costs with the sorting algorithm that we chose for this solution (in-memory Quicksort).


