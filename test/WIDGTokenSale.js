var WIDGTokenSaleMock = artifacts.require("./mocks/WIDGTokenSaleMock.sol");
var WIDGToken = artifacts.require("./WIDGToken.sol");

contract('WIDGTokenSale', function(accounts) {
    var tokenSaleInstance;
    var tokenInstance;

    const numberOfTokensIssued = 1000000;
    const admin = accounts[0];
    const biddingTime = 300; // 5 minutes
    const revealTime = 300; // 5 minutes
    const compareSortingFunctions = true;
    const useManuallyDefinedBids = true;
    const bids = useManuallyDefinedBids ? generateManuallyDefinedBids() : generateSequenceOfValidBids(10);
    const expectedTransactionData = computeTransactionData(bids);

    // Helper functions

    function bid_hash(price, spend, secret) {
        return web3.sha3(
            [price, spend, secret]
            .map(val => web3.toHex(val).slice(2).padStart(64, 0))
            .join(''), 
            { encoding: 'hex' }
        );
    }

    function str2bytes32(str) {
        return '0x' + web3.toHex(str).slice(2).padStart(64, 0);
    }

    function toWei(array) {
        return array.map(eth => web3.toWei(eth));
    }

    function generateManuallyDefinedBids() {

        // object containing all sample bids. Format is "bidder_address: bids_of_bidder"
        let bids = {
            [accounts[0]]: {
                prices: toWei([0.000007]),
                spends: toWei([0.01]),
                secrets: ['xyz'],
                deposits: toWei([0.01])
            },
            [accounts[1]]: {
                prices: toWei([0.000006, 0.000008]),
                spends: toWei([0.005, 0.22]),
                secrets: ['xyz', 'abc123'],
                deposits: toWei([0.005, 0.223])
            },
            [accounts[2]]: {
                prices: toWei([0.0000000001, 0.000009, 0.000002, 0.000005, 0.0]),
                spends: toWei([1.5, 0.11, 0.12, 0.13, 0]),
                secrets: ['u', '', 'x', 'L', ''],
                deposits: toWei([1.5, 0.0, 0.12, 0.15, 0])
            },
            [accounts[3]]: {
                prices: toWei([0.000005, 0.000004, 0.00000299]),
                spends: toWei([0.1, 0.2, 0.0]),
                secrets: ['x', 'x,', 'x'],
                deposits: toWei([0.1, 0.2, 0.1])
            },
        }

        // adding hashes to bids array
        Object.keys(bids).forEach(function(account, index) {
            let _bids = bids[account]
            _bids.hashes = []
            for(let i = 0; i < _bids.prices.length; i++) {
                _bids.hashes.push(bid_hash(
                    _bids.prices[i], _bids.spends[i], _bids.secrets[i]
                ));
            } 
        });

        return bids
    }

    function generateSequenceOfValidBids(n, random = false, minPrice = 0.000001, maxPrice = 0.000009, spendPerBid = 0.0000001) {
        let bids = { [accounts[1]]: { 
                prices: Array.from({ length: n }, 
                    (e,i) => Math.floor(web3.toWei(minPrice + (maxPrice - minPrice) * (random ? Math.random() : (i/(n-1)))))), 
                spends: Array.from({ length: n }, i => web3.toWei(spendPerBid)), 
                deposits: Array.from({ length: n }, i => web3.toWei(spendPerBid)), 
                secrets: Array.from({ length: n }, i => 'x'),
                hashes: []
            }
        }
        let _bids = bids[accounts[1]];
        for(let i = 0; i < n; i++) {
            _bids.hashes.push(bid_hash(_bids.prices[i], _bids.spends[i], _bids.secrets[i]));
        }
        return bids
    }

    function computeTransactionData(bids) {
        bidArray = [];
        for(let [account, _bids] of Object.entries(bids)) {
            for(let i = 0; i < _bids.prices.length; i++) {
                if(parseInt(_bids.deposits[i]) >= parseInt(_bids.spends[i])) {
                    bidArray.push({
                        price: parseInt(_bids.prices[i]),
                        spend: parseInt(_bids.spends[i]),
                        account: account
                    });
                }
            }
        }

        bidArray.sort((a,b) => { return a.price < b.price });

        let valuation = 0, j = 0;
        sells = {};
        refunds = {};
        while(j < bidArray.length && Math.floor((valuation + bidArray[j].spend) / numberOfTokensIssued) <= bidArray[j].price) {
            valuation += bidArray[j].spend;
            sells[bidArray[j].account] = (sells[bidArray[j].account] || 0) + bidArray[j].spend;
            j++;
        }
        while(j < bidArray.length) {
            refunds[bidArray[j].account] = (refunds[bidArray[j].account] || 0) + bidArray[j].spend;
            j++;
        }

        return {
            finalPrice: Math.floor(valuation / numberOfTokensIssued),
            sells: sells,
            refunds: refunds
        };
    }

    // Tests

    it('initializes with the correct values', async () => {
        tokenInstance = await WIDGToken.deployed();
        tokenSaleInstance = await WIDGTokenSaleMock.deployed();
        assert.notEqual(tokenSaleInstance.address, 0x0, 'token sale contract should have contract address');
        const tokenContractAddress = await tokenSaleInstance.tokenContract();
        assert.notEqual(tokenContractAddress, 0x0, 'token sale contract should have token contract address');
    });

    it('lets the admin start the auction', async () => {
        await tokenInstance.transfer(tokenSaleInstance.address, numberOfTokensIssued, { from: admin });
        await tokenSaleInstance.startAuction(biddingTime, revealTime, { from: admin });
    });

    it('stores the bids placed by the users', async () => {
        // check that placing bids correctly stores blinded bids in the contract 
        for(let [account, _bids] of Object.entries(bids)) {
            const _bids = bids[account]
            for(let i = 0; i < _bids.hashes.length; i++) {
                await tokenSaleInstance.placeBid(_bids.hashes[i], { from: account, value: _bids.deposits[i] });

                
                const blindedBid = await tokenSaleInstance.blindedBids(account, i);
                assert.equal(blindedBid[0], _bids.hashes[i], 
                    `Hash ${i} was not stored in the contract for account ${account}`)
                assert.equal(blindedBid[1], _bids.deposits[i], 
                    `Deposit ${i} was not stored in the contract for account ${account}`)
            }
        }
    });

    it('lets users reveal their bids (broken down by chunks) and processes necessary refunds', async () => {
        // travel to the start of the reveal phase
        await tokenSaleInstance.turnBackTime(biddingTime);

        // check that revealing bids leads to correct ether refunds
        for(let [account, _bids] of Object.entries(bids)) {

            const chunkSize = 30;
            for (let startIndex = 0; startIndex < _bids.deposits.length; startIndex += chunkSize) {
                const deposits = _bids.deposits.slice(startIndex, startIndex + chunkSize);
                const prices = _bids.prices.slice(startIndex, startIndex + chunkSize);
                const spends = _bids.spends.slice(startIndex, startIndex + chunkSize);
                const secrets = _bids.secrets.slice(startIndex, startIndex + chunkSize);

                // calculate the expected ether refund for the account
                let refund = 0
                for(let i = 0; i < deposits.length; i++) {
                    let deposit = parseInt(deposits[i]);
                    let spend = parseInt(spends[i]);
                    refund += (deposit < spend) ? deposit : (deposit - spend);
                }

                const initialEtherBalance = web3.eth.getBalance(account);
                const receipt = await tokenSaleInstance.revealBids(
                    prices, spends, secrets.map(str2bytes32), startIndex, { from: account });

                const gasUsed = receipt.receipt.gasUsed;
                const tx = await web3.eth.getTransaction(receipt.tx);
                const gasPrice = tx.gasPrice;
                const finalEtherBalance = web3.eth.getBalance(account);

                assert.equal(
                    finalEtherBalance.add(gasPrice.mul(gasUsed))
                    .minus(refund)
                    .toString(), 
                    initialEtherBalance.toString(), 
                    `Excessive Ether or invalid bids were not correctly refunded for account ${account}`
                );
            }
        }
    });

    it('correctly sorts the revealed bids by descending prices and '
        + 'compares the performance of sorting in storage vs in memory', async () => {
        let receipt = await tokenSaleInstance.sortRevealedBidsWithQuicksortInMemory(true);

        if (compareSortingFunctions) {
            let gasUsed = receipt.receipt.gasUsed;
            console.log(`Gas Used for In-Memory Quicksort Sorting: ${gasUsed}`)
            receipt = await tokenSaleInstance.sortRevealedBidsWithHeapsortInMemory(true);
            gasUsed = receipt.receipt.gasUsed;
            console.log(`Gas Used for In-Memory Heapsort Sorting: ${gasUsed}`)
            receipt = await tokenSaleInstance.sortRevealedBidsWithQuicksortInStorage(true);
            gasUsed = receipt.receipt.gasUsed;
            console.log(`Gas Used for In-Storage Quicksort Sorting: ${gasUsed}`)
        }
    });

    it('lets the admin distribute the right amount of tokens to each successful bidder', async () => {
        // travel to the end of the reveal phase
        await tokenSaleInstance.turnBackTime(revealTime);

        const finalPrice = expectedTransactionData.finalPrice;
        const receipt = await tokenSaleInstance.distributeTokens({ from: admin });
        const numEvents = receipt.logs.length;
        assert.equal(numEvents, Object.keys(expectedTransactionData.sells).length, 'should trigger one event per sale');
        
        for(let i = 0; i < numEvents; i++) {
            assert.equal(receipt.logs[i].event, 'Sell', 'should trigger a Sell event');
            assert.equal(receipt.logs[i].args._price.toNumber(), finalPrice, 'should log the correct sale price');
            const buyer = receipt.logs[i].args._buyer;
            const expectedEtherSpent = expectedTransactionData.sells[buyer];
            const expectedAmountOfTokensReceived = Math.floor(expectedEtherSpent / finalPrice);
            assert.equal(receipt.logs[i].args._amount.toNumber(), expectedAmountOfTokensReceived, 'the correct number of tokens should be sent to the buyer');
            const tokenBalance = await tokenInstance.balanceOf(buyer);
            if (buyer == admin) {
                // if the bidder is the admin, his token balance may be slightly greater than
                // what he bidded because the admin receives the remaining tokens (resulting from rounding errors)
                assert.isAtLeast(tokenBalance.toNumber(), expectedAmountOfTokensReceived, 'the token balance of the admin should be accurate');
            } else {
                assert.equal(tokenBalance.toNumber(), expectedAmountOfTokensReceived, 'the token balance of the buyer should be accurate');
            }
        }
    });

    it('lets the unsuccessful bidders withdraw their ether', async () => {
        for(let [account, refund] of Object.entries(expectedTransactionData.refunds)) {
            const initialEtherBalance = web3.eth.getBalance(account);
            const receipt = await tokenSaleInstance.withdrawRefund({ from: account });

            const gasUsed = receipt.receipt.gasUsed;
            const tx = await web3.eth.getTransaction(receipt.tx);
            const gasPrice = tx.gasPrice;
            const finalEtherBalance = web3.eth.getBalance(account);

            assert.equal(
                finalEtherBalance.add(gasPrice.mul(gasUsed))
                .minus(refund)
                .toString(), 
                initialEtherBalance.toString(), 
                `Unsuccessful bids were not correctly refunded for account ${account}`
            );
        }
    });
});









