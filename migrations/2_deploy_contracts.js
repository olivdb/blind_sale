var WIDGToken = artifacts.require("./WIDGToken.sol");
var WIDGTokenSaleMock = artifacts.require("./mocks/WIDGTokenSaleMock.sol");

module.exports = function(deployer) {

    var totalSupply = 1000000;

    deployer.deploy(WIDGToken, totalSupply).then(function() {
        return deployer.deploy(
            WIDGTokenSaleMock, 
            WIDGToken.address
        );
    });  
};
