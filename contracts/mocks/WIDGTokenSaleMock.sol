pragma solidity ^0.4.23;

import "../WIDGTokenSale.sol";

contract WIDGTokenSaleMock is WIDGTokenSale {
    constructor(WIDGToken _tokenContract) WIDGTokenSale(_tokenContract) public {}

    function turnBackTime(uint256 secs) 
    external 
    {
        biddingEnd -= secs;
        revealEnd -= secs;
    }

    //
    // Compare gas usage of Quicksort in-memory, Quicksort in-storage and Heapsort in-memory
    //

    event DidSort(); // This event is solely used to silence the warnings that suggest to mark sortRevealedBidsWithQuicksortInMemory() 
    // and sortRevealedBidsWithHeapsortInMemory() as "view" since these functions don't change the state of the contract. 
    // However, if we did so, we woulnd't be able to call the functions with a transaction and retrieve the corresponding gas usage.

    function sortRevealedBidsWithQuicksortInMemory(bool checkSorted) 
    external 
    {
        RevealedBid[] memory _sortedBids = _computeSortedBids(revealedBids, 0, revealedBids.length - 1, _descendingPrices);
        if (checkSorted) {
            for (uint i = 0; i < _sortedBids.length - 1; i++) {
                if(_sortedBids[i].price < _sortedBids[i+1].price) {
                    // prices are not sorted
                    revert('The revealed bids were not sorted properly');
                }
            }
        }
        emit DidSort();
    }

    function sortRevealedBidsWithQuicksortInStorage(bool checkSorted) 
    external 
    {
        _computeSortedBidsWithQuicksortInStorage(revealedBids, 0, revealedBids.length - 1, _descendingPricesStorage);
        if (checkSorted) {
            for (uint i = 0; i < revealedBids.length - 1; i++) {
                if(revealedBids[i].price < revealedBids[i+1].price) {
                    // prices are not sorted
                    revert('The revealed bids were not sorted properly');
                }
            }
        }
    }

    function sortRevealedBidsWithHeapsortInMemory(bool checkSorted) 
    external 
    {
        RevealedBid[] memory _sortedBids = _computeSortedBidsWithHeapsortInMemory(revealedBids, _descendingPrices);
        if (checkSorted) {
            for (uint i = 0; i < _sortedBids.length - 1; i++) {
                if(_sortedBids[i].price < _sortedBids[i+1].price) {
                    // prices are not sorted
                    revert('The revealed bids were not sorted properly');
                }
            }
        }
        emit DidSort();
    }

    /// Uses quicksort to sort the revealed bids in storage 
    function _computeSortedBidsWithQuicksortInStorage(
        RevealedBid[] storage _arr, uint _left, uint _right, 
        function (RevealedBid storage, RevealedBid storage) view internal returns (bool) _less)  
    internal 
    {
        uint i = _left;
        uint j = _right;
        if(i == j) return;
        RevealedBid storage pivot = _arr[_left + (_right - _left) / 2];
        
        while (i <= j) {
            while (_less(_arr[i], pivot)) i++;
            while (_less(pivot, _arr[j])) j--;
            if (i <= j) {
                // using a temporary variable in memory consumes
                // ~44% less gas than using a temporary storage variable
                RevealedBid memory tmp = _arr[i]; 
                _arr[i] = _arr[j];
                _arr[j] = tmp;
                i++;
                j = (j > 0) ? j-1 : 0;
            }
        }
        if (_left < j)
        _computeSortedBidsWithQuicksortInStorage(_arr, _left, j, _less);
        if (i < _right)
        _computeSortedBidsWithQuicksortInStorage(_arr, i, _right, _less);
    }

    /// Comparison function, used as a parameter to _computeSortedBidsWithQuicksortInStorage()
    function _descendingPricesStorage(RevealedBid storage a, RevealedBid storage b) 
    view 
    internal returns (bool) 
    {
        return a.price > b.price;
    }

    /// Uses heapsort to sort the revealed bids in memory
    function _computeSortedBidsWithHeapsortInMemory(
        RevealedBid[] memory _arr, 
        function (RevealedBid memory, RevealedBid memory) pure internal returns (bool) _less) 
    pure 
    internal 
    returns (RevealedBid[]) 
    {
        uint N = _arr.length;
        for (uint k = N/2; k >= 1; k--)
        _sink(_arr, k, N, _less);
        while (N > 1) {
            _exch(_arr, 0, (N--)-1);
            _sink(_arr, 1, N, _less);
        }

        return _arr;
    }

    /// Function used by heapsort
    function _sink(
        RevealedBid[] memory _arr,
        uint k,
        uint N,
        function (RevealedBid memory, RevealedBid memory) pure internal returns (bool) _less)
    pure
    private
    {
        while (2*k <= N) {
            uint j = 2*k;
            if (j < N && _less(_arr[j-1], _arr[j])) j++;
            if (!_less(_arr[k-1], _arr[j-1])) break;
            _exch(_arr, k-1, j-1);
            k = j;
        } 
    }

    /// Function used by heapsort
    /// Swaps _arr[i] and _arr[j]
    function _exch(
        RevealedBid[] memory _arr,
        uint i,
        uint j)
        pure
        private
    {
        RevealedBid memory tmp = _arr[i]; 
        _arr[i] = _arr[j];
        _arr[j] = tmp;
    }

}