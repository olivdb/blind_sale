pragma solidity ^0.4.23;

import "./WIDGToken.sol";

contract WIDGTokenSale {
    struct BlindedBid {
        bytes32 hash;
        uint deposit;
    }

    struct RevealedBid {
        address bidder;
        uint price;
        uint etherSpent;
    }

    event Sell(address _buyer, uint _amount, uint _price);

    address public admin;
    WIDGToken public tokenContract;

    uint public numberOfTokensInSale;
    bool public started; // becomes true after admin calls startAuction()
    uint public biddingEnd; // timestamp as seconds since unix epoch 
    uint public revealEnd; // timestamp as seconds since unix epoch
    bool public ended; // becomes true after admin calls distributeTokens()

    mapping(address => BlindedBid[]) public blindedBids;
    RevealedBid[] public revealedBids;
    mapping(address => uint) pendingReturns; // Allowed withdrawals of unsuccessful bids

    modifier onlyBefore(uint _time) { require(now <= _time, 'Too late to call this function'); _; }
    modifier onlyAfter(uint _time) { require(now >= _time, 'Too early to call this function'); _; }
    modifier onlyAdmin() { require(msg.sender == admin, 'Only admin can call this function'); _; }
    modifier onlyAfterStart() { require(started, 'This function can only be called after auction was started'); _; }
    modifier onlyBeforeEnd() { require(!ended, 'This function can only be called before auction was ended'); _; }
    modifier onlyAfterEnd() { require(ended, 'This function can only be called after auction was ended'); _; }

    ///
    /// Public functions
    ///

    constructor(WIDGToken _tokenContract) public {
        admin = msg.sender;
        tokenContract = _tokenContract;
    }

    function startAuction(
        uint _biddingTime,
        uint _revealTime) 
        public
        onlyAdmin
    {
        require(!started);
        uint _numberOfTokensInSale = tokenContract.balanceOf(this);
        require(_numberOfTokensInSale > 0, 
            'You must provision at least one token to the sale contract');
        numberOfTokensInSale = _numberOfTokensInSale;

        biddingEnd = now + _biddingTime;
        revealEnd = biddingEnd + _revealTime;
        started = true;
    }

    /// Place a blinded bid with `_hash` = keccak256(price,
    /// etherSpent, secret).
    /// The sent ether is only refunded if the bid is correctly
    /// revealed in the revealing phase. The bid is valid if the
    /// ether sent together with the bid is at least "etherSpent". 
    /// Sending not the exact amount is a way to hide the real bid. 
    /// The same address can place multiple bids.
    function placeBid(bytes32 _hash)
        public
        payable
        onlyAfterStart
        onlyBefore(biddingEnd)
    {
        blindedBids[msg.sender].push(BlindedBid({
            hash: _hash,
            deposit: msg.value
            }));
    }

    /// Reveal your blinded bids. You will get a refund for all
    /// correctly blinded invalid bids and for any excessive ether 
    /// sent with your correctly blinded valid bids.
    function revealBids(
        uint[] _prices,
        uint[] _etherSpent,
        bytes32[] _secrets,
        uint startIndex)
        public
        onlyAfterStart
        onlyAfter(biddingEnd)
    {
        uint totalNumberOfBids = blindedBids[msg.sender].length;
        uint numberOfReveals = _prices.length;
        require(startIndex + numberOfReveals <= totalNumberOfBids);
        require(_etherSpent.length == numberOfReveals);
        require(_secrets.length == numberOfReveals);

        uint refund;
        for (uint i = 0; i < numberOfReveals; i++) {
            BlindedBid storage bid = blindedBids[msg.sender][startIndex + i];
            (uint price, uint etherSpent, bytes32 secret) =
                (_prices[i], _etherSpent[i], _secrets[i]);
            
            if (bid.hash != keccak256(
                abi.encodePacked(price, etherSpent, secret))) {
                // Bid was not actually revealed.
                // Do not refund deposit.
                continue;
            }
            refund += bid.deposit;
            if (!ended && bid.deposit >= etherSpent && etherSpent > 0) {
                revealedBids.push(RevealedBid({
                    bidder: msg.sender, 
                    price: price, 
                    etherSpent: etherSpent
                }));
                refund -= etherSpent;
            }
            // Make it impossible for the sender to re-claim
            // the same deposit.
            bid.hash = bytes32(0);
        }
        msg.sender.transfer(refund);
    }

    function distributeTokens() 
        public 
        onlyAdmin
        onlyAfterStart
        onlyAfter(revealEnd) 
        onlyBeforeEnd
    {
        require(revealedBids.length > 0);

        RevealedBid[] memory _sortedBids = _computeSortedBids(revealedBids, 0, revealedBids.length - 1, _descendingPrices);
        uint valuation;
        uint numberOfBuys;
        uint salePrice;
        (valuation, numberOfBuys, salePrice) = _computeSaleParameters(_sortedBids, numberOfTokensInSale);
        uint numberOfBuyers = _performTokenDistribution(_sortedBids, numberOfBuys, salePrice);
        _refundLosingBids(_sortedBids, numberOfBuys);

        // there can at most be one token left per buyer (due to rounding errors)
        uint remainingTokens = tokenContract.balanceOf(this);
        assert(remainingTokens <= numberOfBuyers);

        // send remaining tokens to the admin
        tokenContract.transfer(admin, remainingTokens);

        // send the ether spent to the admin
        admin.transfer(valuation);

        // ending the sale. Unsuccessful bidders can now withdraw their deposits
        ended = true;
    }

    /// Withdraw the ether deposited for 
    /// bids that were unsuccessful (i.e. their max prices 
    /// were lower than the sale price).
    function withdrawRefund() 
        public 
        onlyAfterEnd
    {
        uint amount = pendingReturns[msg.sender];
        if (amount > 0) {
            pendingReturns[msg.sender] = 0;

            msg.sender.transfer(amount);
        }
    }

    ///
    /// Internal functions
    ///


    /// Integer division of two numbers, truncating the quotient.
    function _div(uint a, uint b) 
        internal 
        pure 
        returns (uint) 
    {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        // uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return a / b;
    }

    /// Uses quicksort to sort bids, in memory
    function _computeSortedBids(
        RevealedBid[] memory _arr, 
        uint _left, 
        uint _right, 
        function (RevealedBid memory, RevealedBid memory) pure internal returns (bool) _less) 
        pure 
        internal 
        returns (RevealedBid[]) 
    {
        uint i = _left;
        uint j = _right;
        if (i == j) return _arr;
        RevealedBid memory pivot = _arr[_left + (_right - _left) / 2];

        while (i <= j) {
            while (_less(_arr[i], pivot)) i++;
            while (_less(pivot, _arr[j])) j--;
            if (i <= j) {
                (_arr[i], _arr[j]) = (_arr[j], _arr[i]);
                i++;
                j = (j > 0) ? j-1 : 0;
            }
        }
        if (_left < j)
        _arr = _computeSortedBids(_arr, _left, j, _less);
        if (i < _right)
        _arr = _computeSortedBids(_arr, i, _right, _less);

        return _arr;
    }

    // Sorting function, used as a parameter to _computeSortedBids()
    function _descendingPrices(
        RevealedBid memory a, 
        RevealedBid memory b) 
        pure 
        internal 
        returns (bool) 
    {
        return a.price > b.price;
    }

    /// Sorting function, used as a parameter to _computeSortedBids()
    function _ascendingBidders(
        RevealedBid memory a, 
        RevealedBid memory b) 
        pure 
        internal 
        returns (bool) 
    {
        return a.bidder < b.bidder;
    }

    /// Computes the valuation of the sale (i.e. total amount of 
    /// ether taken in exchange for the tokens), the number of 
    /// successful bids _numberOfBuys and the sale price.
    function _computeSaleParameters(
        RevealedBid[] memory _sortedBids, 
        uint _numberOfTokensInSale) 
        pure 
        internal 
        returns (uint _valuation, uint _numberOfBuys, uint _salePrice) 
    {
        // calculate the maximum possible valuation, i.e. the maximum amount of ether that can be raised in the ICO
        uint j = 0;
        while(j < _sortedBids.length && _div(_valuation + _sortedBids[j].etherSpent, _numberOfTokensInSale) <= _sortedBids[j].price) {
            _valuation += _sortedBids[j].etherSpent;
            j++;
        }

        // make sure that there was at least one valid bid with a sufficiently high price
        require(j >= 1); 

        _numberOfBuys = j;
        _salePrice = _div(_valuation, _numberOfTokensInSale);
    }

    function _sendTokens(
        address _bidder, 
        uint _etherSpent, 
        uint _salePrice) 
        internal 
    {
        uint numTokens = _div(_etherSpent, _salePrice);
        tokenContract.transfer(_bidder, numTokens);
        emit Sell(_bidder, numTokens, _salePrice);
    }

    function _performTokenDistribution(
        RevealedBid[] memory _sortedBids, 
        uint _numberOfBuys, 
        uint _salePrice) 
        internal 
        returns (uint _numberOfBuyers)  
    {
        // sort the "winning" bids by bidder addresses
        _computeSortedBids(_sortedBids, 0, _numberOfBuys - 1, _ascendingBidders);

        // aggregate the total ether spent for each bidder
        // and send them the corresponding number of tokens
        address bidder;
        uint totalEtherSpentByBidder;
        for(uint j = 0; j < _numberOfBuys; j++) {
            if(bidder != _sortedBids[j].bidder) {
                if (bidder != address(0)) {
                    // send tokens to previous bidder
                    _sendTokens(bidder, totalEtherSpentByBidder, _salePrice);
                    _numberOfBuyers++;
                }
                bidder = _sortedBids[j].bidder;
                totalEtherSpentByBidder = 0;
            } 
            totalEtherSpentByBidder += _sortedBids[j].etherSpent;
        }
        // send tokens to last bidder
        _sendTokens(bidder, totalEtherSpentByBidder, _salePrice);
        _numberOfBuyers++;
    }

    function _refundLosingBids(
        RevealedBid[] memory _sortedBids, 
        uint _numberOfBuys) 
        internal 
    {
        for(uint j = _numberOfBuys; j < _sortedBids.length; j++) {
            pendingReturns[_sortedBids[j].bidder] += _sortedBids[j].etherSpent;
        }
    }
}






